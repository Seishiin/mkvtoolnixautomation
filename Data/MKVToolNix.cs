﻿using System.IO;
using System.Runtime.InteropServices;
using MKVToolNixAutomation.Enums;

namespace MKVToolNixAutomation.Data;

public class MKVToolNix
{
	private string? directory;

	private static string MergeFile => RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "mkvmerge.exe" : "mkvmerge";
	private static string PropEditFile => RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "mkvpropedit.exe" : "mkvpropedit";

	public string? Directory
	{
		get => directory;
		set => SetDirectory(value);
	}

	public string? Merge { get; private set; }

	public string? PropEdit { get; private set; }

	private void SetDirectory(string? dir)
	{
		if (dir == null) throw new MKVException(ErrorType.MKVToolNixNotSet);
		string merge = Path.Combine(dir, MergeFile);
		string propEdit = Path.Combine(dir, PropEditFile);
		if (!File.Exists(merge) || !File.Exists(propEdit)) throw new MKVException(ErrorType.MKVToolNixInvalid);

		directory = dir;
		Merge = merge;
		PropEdit = propEdit;
	}
}