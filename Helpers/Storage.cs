﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Platform.Storage;

namespace MKVToolNixAutomation.Helpers;

public static class Storage
{
	public static async Task<string[]> SelectFiles(string name, string extension, bool allowMultiple)
	{
		var options = new FilePickerOpenOptions
		{
			AllowMultiple = allowMultiple,
			FileTypeFilter = new[]
			{
				new FilePickerFileType(name) { Patterns = new[] { $"*.{extension}" } },
			},
		};

		if (Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime { MainWindow: { } window })
		{
			var files = await window.StorageProvider.OpenFilePickerAsync(options);
			return files.Select(f => f.Path.LocalPath).ToArray();
		}

		return Array.Empty<string>();
	}

	public static async Task<string?> SelectFolder()
	{
		var options = new FolderPickerOpenOptions { AllowMultiple = false };
		if (Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime { MainWindow: { } window })
		{
			var folders = await window.StorageProvider.OpenFolderPickerAsync(options);
			if (folders.Count > 0) return folders[0].Path.LocalPath;
		}

		return null;
	}
}