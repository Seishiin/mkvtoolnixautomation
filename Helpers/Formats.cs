﻿using System.Collections.Generic;
using System.IO;

namespace MKVToolNixAutomation.Helpers;

public static class Formats
{
	private static readonly HashSet<string> Fonts = new() { ".otf", ".ttf", ".fnt", ".ttc" };
	private static readonly HashSet<string> Subtitles = new() { ".sub", ".sup", ".srt", ".ssa", ".ass", ".idx", ".usf" };

	public static bool IsFontFile(string fileName)
	{
		return Fonts.Contains(Path.GetExtension(fileName).ToLowerInvariant());
	}

	public static bool IsSubtitleFile(string fileName)
	{
		return Subtitles.Contains(Path.GetExtension(fileName).ToLowerInvariant());
	}
}