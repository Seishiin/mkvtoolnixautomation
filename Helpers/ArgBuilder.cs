﻿using System.IO;
using System.Text;
using MKVToolNixAutomation.Models;

namespace MKVToolNixAutomation.Helpers;

public static class ArgBuilder
{
	public static string Generate(ContainerAttachments attachments, string? optionalArgs, string outputDirectory)
	{
		var args = new StringBuilder();
		string outputFile = Path.Combine(outputDirectory, Path.GetFileName(attachments.Video)!);

		args.Append($"-o \"{outputFile}\"");

		if (attachments.Chapters != null)
		{
			args.Append($" --chapters \"{attachments.Chapters}\"");
		}

		if (attachments.Tags != null)
		{
			args.Append($" --tags 0:\"{attachments.Tags}\"");
		}

		if (!string.IsNullOrEmpty(optionalArgs))
		{
			args.Append($" {optionalArgs}");
		}

		foreach (string subtitle in attachments.Subtitles)
		{
			args.Append($" \"{subtitle}\"");
		}

		foreach (string font in attachments.Fonts)
		{
			args.Append($" --attach-file \"{font}\"");
		}

		args.Append($" \"{attachments.Video}\"");

		return args.ToString();
	}

	public static string Generate(ContainerMetadata metadata, Track? video, Track? audio, Track? subtitles, bool force)
	{
		var args = new StringBuilder();
		string forced = force ? "1" : "0";

		args.Append($"\"{metadata.FilePath}\"");

		foreach (var track in metadata.Tracks)
		{
			args.Append($" --edit track:@{track.Properties.Number} --set flag-default=0 --set flag-forced=0");
		}

		if (video is { IsEmpty: false })
		{
			args.Append($" --edit track:@{video.Properties.Number} --set flag-default=1 --set flag-forced={forced}");
		}

		if (audio is { IsEmpty: false })
		{
			args.Append($" --edit track:@{audio.Properties.Number} --set flag-default=1 --set flag-forced={forced}");
		}

		if (subtitles is { IsEmpty: false })
		{
			args.Append($" --edit track:@{subtitles.Properties.Number} --set flag-default=1 --set flag-forced={forced}");
		}

		return args.ToString();
	}
}