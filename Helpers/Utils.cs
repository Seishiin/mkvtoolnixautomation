﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MKVToolNixAutomation.Helpers;

public static partial class Utils
{
	[GeneratedRegex("((\\sEP.?\\d+\\s?)|((\\s-)?\\s\\d+\\s(END\\s?)?))", RegexOptions.IgnoreCase, "en-US")]
	private static partial Regex Pattern();

	public static bool ContainsSingleDirectory(DirectoryInfo dir)
	{
		return dir.GetDirectories().Length == 1;
	}

	public static bool ContainsFontFiles(DirectoryInfo dir)
	{
		return dir.GetFiles().Any(file => Formats.IsFontFile(file.Name));
	}

	public static bool ContainsFontFiles(string dir)
	{
		return Directory.GetFiles(dir).Any(Formats.IsFontFile);
	}

	public static string GenerateOutputDirectoryName(string filePath)
	{
		string s = Path.GetFileNameWithoutExtension(filePath);
		return $"[Output] {Pattern().Replace(s, " ")}";
	}
}