﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using DynamicData.Binding;
using MKVToolNixAutomation.Helpers;
using ReactiveUI;

namespace MKVToolNixAutomation.ViewModels;

public class AddAttachmentsViewModel : ViewModelBase, ITabbed
{
	public string TabHeader => "Add attachments";

	private readonly ObservableAsPropertyHelper<string> videosString;
	public string VideosString => videosString.Value;

	private string? attachmentsDirectoryPath;

	public string? AttachmentsDirectoryPath
	{
		get => attachmentsDirectoryPath;
		private set => this.RaiseAndSetIfChanged(ref attachmentsDirectoryPath, value);
	}

	private string? chaptersFileName = "chapters.xml";

	public string? ChaptersFileName
	{
		get => chaptersFileName;
		set => this.RaiseAndSetIfChanged(ref chaptersFileName, value);
	}

	private string? tagsFileName = "tags.xml";

	public string? TagsFileName
	{
		get => tagsFileName;
		set => this.RaiseAndSetIfChanged(ref tagsFileName, value);
	}

	private string? optionalArguments;

	public string? OptionalArguments
	{
		get => optionalArguments;
		set => this.RaiseAndSetIfChanged(ref optionalArguments, value);
	}

	private string[] videoFilePaths = Array.Empty<string>();

	public string[] VideoFilePaths
	{
		get => videoFilePaths;
		private set
		{
			if (value.Any(string.IsNullOrWhiteSpace) || value.SequenceEqual(videoFilePaths)) return;
			videoFilePaths = value;
			this.RaisePropertyChanged();
		}
	}

	public AddAttachmentsViewModel()
	{
		videosString =
			this.WhenValueChanged(x => x.VideoFilePaths)
			    .Select(files => string.Join(", ", (files ?? Array.Empty<string>()).Select(Path.GetFileName)))
			    .ToProperty(this, x => x.VideosString, string.Empty);
	}

	public async Task SelectVideos()
	{
		string[] files = await Storage.SelectFiles("Matroska files", "mkv", true);
		if (files.Any()) VideoFilePaths = files;
	}

	public async Task SelectAttachments()
	{
		string? folder = await Storage.SelectFolder();
		if (!string.IsNullOrEmpty(folder)) AttachmentsDirectoryPath = folder;
	}
}