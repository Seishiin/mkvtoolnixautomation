using Avalonia.Controls;
using MKVToolNixAutomation.Helpers;

namespace MKVToolNixAutomation.Views;

public partial class MainWindow : Window
{
	public MainWindow()
	{
		InitializeComponent();
		Logger.Initialize(Log);
	}
}