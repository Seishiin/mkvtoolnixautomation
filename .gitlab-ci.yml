variables:
  RUNTIME_IDENTIFIERS: 'win-x64 linux-x64'
  BUILD_OPTIONS: '--self-contained false -p:PublishSingleFile=true -p:IncludeNativeLibrariesForSelfExtract=true'
  PACKAGES_URL: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}'

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /[Rr]elease/
      when: always

stages:
  - build
  - upload
  - release

build:
  stage: build
  image: mcr.microsoft.com/dotnet/sdk:latest
  script:
    - set -euo pipefail
    - dotnet restore --packages .nuget
    - version=$(date -u +%Y.%m.%d)_$(($(date -u +%s) - $(date -u -d "today 0" +%s)))
    - assets=""
    - >
      for RID in ${RUNTIME_IDENTIFIERS}; do
        assets+="--assets-link {\"name\":\"${RID}\",\"url\":\"${PACKAGES_URL}/${version}/${CI_PROJECT_NAME}_${RID}.tar\"} "
        dotnet publish -c Release -r "$RID" $BUILD_OPTIONS /p:DebugType=None
        mkdir -p "binaries/${RID}"
        for file in bin/Release/*/"${RID}"/publish/*; do mv "$file" "binaries/${RID}"; done
      done
    - echo "VERSION=$version" >> build.env
    - echo "ASSETS=$assets" >> build.env
  artifacts:
    expire_in: 1 hour
    paths:
      - binaries/
    reports:
      dotenv: build.env

upload:
  stage: upload
  image: curlimages/curl:latest
  needs:
    - job: build
      artifacts: true
  script:
    - >
      for RID in ${RUNTIME_IDENTIFIERS}; do
        TAR_FILE="${CI_PROJECT_NAME}_${RID}.tar"
        tar -cvf "$TAR_FILE" -C "binaries/${RID}" .
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "$TAR_FILE" "${PACKAGES_URL}/${VERSION}/${TAR_FILE}"
      done

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: build
      artifacts: true
  script:
    - release-cli create --name "${VERSION}" --tag-name "${VERSION}"
      --description "[Pipeline](${CI_PIPELINE_URL})" --ref "${CI_COMMIT_SHA}" $ASSETS