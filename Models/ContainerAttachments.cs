﻿using System.Collections.Generic;

namespace MKVToolNixAutomation.Models;

public class ContainerAttachments
{
	public string? Video { get; set; }
	public string? Chapters { get; }
	public string? Tags { get; }
	public List<string> Subtitles { get; }
	public List<string> Fonts { get; }

	public ContainerAttachments(string? chapters, string? tags, List<string> subtitles, List<string> fonts)
	{
		Chapters = chapters;
		Tags = tags;
		Subtitles = subtitles;
		Fonts = fonts;
	}

	public ContainerAttachments(string subtitles, List<string> fonts)
	{
		Subtitles = new List<string> { subtitles };
		Fonts = fonts;
	}
}